#include "pch.h"
#include <iostream>
#include <list>
#include <string>
using namespace System;
using namespace std;

struct Persoana
{
    unsigned vs;
    char nume[20];
    char prenume[20];
};

class Pers {
    private:
        unsigned vs;
        char nume[20];
        char prenume[20];
public:
    Pers(unsigned vs1, char* num1, char* prenum1) {
        vs = vs1;
        strcpy_s(nume, num1);
        strcpy_s(prenume, prenum1);
    }
    Pers() {}
    unsigned arata_vs() { return this->vs; }
    char* arata_nume() { return this->nume; }
    char* arata_prenume() { return this->prenume; }
};

int main(/* array<System::String ^> ^args */)
{
    std::cout << "Introdu nr de persoane:";
    std::string persoane;
    std::getline(std::cin, persoane);
    std::list<Pers> listaPersoane;



    for (int i = 0; i < stoi(persoane); i++) {
        unsigned vs;
        char nume[20];
        char prenume[20];
        std::cout << "Nume: ";
        std::cin >> nume;
        std::cout << "Prenume: ";
        std::cin >> prenume;
        std::cout << "Varstas: ";
        std::cin >> vs;
        Pers pers =  Pers(vs, nume, prenume);
        listaPersoane.push_back(pers);
        std::cout << "\n";
    }

    for (auto& persoana : listaPersoane) {
        cout << "Ma numesc " << persoana.arata_nume() << " " << persoana.arata_prenume() << " si am " << persoana.arata_vs() << " ani." << endl;

    }

    return 0;
}
