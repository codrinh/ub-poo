#include "pch.h"
using namespace std;
using namespace System;

ref class ShoppingItem {
private:
    String^ name;
    double price;
    int quantity;

public:
    ShoppingItem(String^ itemName, double itemPrice, int itemQuantity)
        : name(itemName), price(itemPrice), quantity(itemQuantity) {}

    String^ GetName() {
        return name;
    }

    double GetPrice() {
        return price;
    }

    int GetQuantity() {
        return quantity;
    }

    double GetTotalAmount() {
        return price * quantity;
    }
};
