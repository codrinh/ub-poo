#include "pch.h"
#include "ShoppingItem.cpp"
using namespace std;
using namespace System;
using namespace System::Collections::Generic;


ref class ShoppingList {
private:
    List<ShoppingItem^>^ items;

public:
    ShoppingList() {
        items = gcnew List<ShoppingItem^>();
    }

    void AddItem(ShoppingItem^ item) {
        items->Add(item);
    }

    void RemoveItem(int index) {
        if (index >= 0 && index < items->Count) {
            items->RemoveAt(index);
        }
    }

    void DisplayList() {
        if (items->Count == 0) {
            Console::WriteLine("Lista este goala.");
            return;
        }

        Console::WriteLine("\nLista cumparaturi:");

        for each (ShoppingItem ^ item in items) {
            Console::WriteLine(item->GetName() + ": " + item->GetPrice() + " Lei x " +
                item->GetQuantity() + " = " + item->GetTotalAmount() + "  Lei");
        }
    }
};


