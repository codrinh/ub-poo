#include "pch.h"
#include "ShoppingList.cpp"

using namespace std;
using namespace System;


int main(array<System::String^>^ args)
{
    ShoppingList^ shoppingList = gcnew ShoppingList();

    while (true) {
        Console::WriteLine("Selecteaza o actiune:\n");
        Console::WriteLine("1. Adauga in lista");
        Console::WriteLine("2. Sterge din lista");
        Console::WriteLine("3. Afiseaza lista");
        Console::WriteLine("4. Iesire\n");
        Console::Write("Indrodu nr. optiunii: ");

        int choice;
        if (!Int32::TryParse(Console::ReadLine(), choice)) {
            Console::WriteLine("Optiune invalida!");
            continue;
        }

        switch (choice) {
        case 1: {
            Console::Write("Introdu numele: ");
            String^ name = Console::ReadLine();

            double price;
            Console::Write("Introdu pretul: ");
            if (!Double::TryParse(Console::ReadLine(), price)) {
                Console::WriteLine("Optiune invalida!");
                continue;
            }

            int quantity;
            Console::Write("Introdu cantitatea: ");
            if (!Int32::TryParse(Console::ReadLine(), quantity)) {
                Console::WriteLine("Cantite incorecta.");
                continue;
            }

            ShoppingItem^ item = gcnew ShoppingItem(name, price, quantity);
            shoppingList->AddItem(item);

            Console::WriteLine("Lista de cumpraturi a fostt actualizata.");
            break;

        }
        case 2: {
            Console::Write("Introdu indexul obiectului pe care vrei sa il stergi: ");
            int index;
            if (!Int32::TryParse(Console::ReadLine(), index)) {
                Console::WriteLine("Index invalid.");
                continue;
            }

            shoppingList->RemoveItem(index);
            Console::WriteLine("Obiect sters din lista de cumpraturi");
            break;
        }
        case 3:
            shoppingList->DisplayList();
            break;
        case 4:
            return 0;
        default:
            Console::WriteLine("Optiune invalida!");
            break;
        }
        
        Console::WriteLine("\n-------------------");
    }

    return 0;
}